extends Control

signal cursor_off
export var in_debug_mode: bool = false

const RECOVERY_STORAGE_STR = "(rexovery storage)"

# The followimg are the main files we care about tracking
var resource_regexes = [
	"(?i)\\w+\\.png",
	"\\w+\\.webm",
	"(?i)\\w+\\.jpg",
	"\\w+\\.tscn",
	"\\w+\\.gd",
	"\\w+\\.ogg",
	"\\w+\\.godot",
	"\\w+\\.tres",
	"\\w+\\.gif",
	"\\w+\\.material",
	"\\w+\\.gltf",
	"(?i)\\w+\\.svg",
	"\\w+\\.wav" #, "\\^[\\w\\-. ]+$
	]
	
# The following are the sorts of files we never want to track
var exclude_resource_files_regexes = [
	"\\w+\\.import"
	]
	
# The following are the files that will never be orphaned
var never_orphaned_files = [
	"res://project.godot"
	]
	
# The following are files that we will not ever scan through to find content
var exclude_scanning_files_regexes = [
	"(?i)\\w+\\.png",
	"(?i)\\w+\\.jpg",
	"\\w+\\.import",
	"(?i)\\w+\\.ogg",
	"(?i)\\w+\\.wav",
	"(?i)\\w+\\.gif",
	"(?i)\\w+\\.material",
	"(?i)\\w+\\.gltf"
	]

	
var resources_file_list: Dictionary = {}

#var resource_holding_pen = "user://resource_holding_pen/"
var resource_holding_pen = "/home/conz/Downloads/godot_unusued_resource_scanner_recovery/"
var regex = RegEx.new()

onready var DataExtractionThread: Thread

onready var OptionsDialog = preload("res://scenes/Options.tscn")
onready var data_extraction_progress_timer = Timer.new()

var godot_project_path : String = "" # Gives us something like "/home/conz/Documents/development/MyProject/"
var godot_project_name: String = ""

# Options
export var substring_search_sensitivity: int = 7
export var godot_projects_base_directory: String = ""
export var recovery_storage_base_directory: String = ""

# Search time computetion 'averages', used for iaverage_search_time_100_filesnterpolation computation
var average_search_time_100_files: float
var average_search_time_1000_files: float
var average_search_time_10000_files: float

onready var ResourceList = self.find_node("ResourceList")
onready var DataProgressBar = self.find_node("DataProgressBar")
onready var ProjectDirectoryChooser = self.find_node("ProjectDirectoryChooser")
onready var ProjectName = self.find_node("ProjectName")
onready var HourglassCursor = preload("res://scenes/HourglassCursor.tscn")
onready var ImagePreview = self.find_node("ImagePreview")
onready var ResourcesForRemoval = self.find_node("ResourcesForRemoval")
onready var RemovalLabel = self.find_node("RemovalLabel")

func _ready():
	LoadOptions()
	DataProgressBar.visible = false
	ResourcesForRemoval.show_line_numbers = false
	$ProjectDirectoryChooser.current_dir = godot_projects_base_directory
	$RecoveryFileChooser.current_dir = recovery_storage_base_directory

func _input(event):
	if event is InputEventKey:
		if Input.is_action_just_pressed("ui_quit"):
			SaveAndQuit()

func _on_Close_pressed():
	SaveAndQuit()
	
	
func SaveAndQuit():
	SaveOptions()
	get_tree().quit()


func _on_SelectGodotProject_pressed():
	$ProjectDirectoryChooser.popup_centered()


func _on_TextEdit_breakpoint_toggled(row):
	print("breakpoint line:", row )


func _on_TextEdit_info_clicked(row, info):
	print("_on_TextEdit_info_clicked: ", row)


func _on_ProjectDirectoryChooser_dir_selected(godot_project_dir):
	DataExtractionThread = Thread.new()
	godot_project_path = godot_project_dir + "/"
	
	ProjectDirectoryChooser.hide()
	# Let's first confirm that this is a godot prooject and that we can read it
	if ConfirmGodotProject(godot_project_path):
		godot_project_name = GetGodotProjectName(godot_project_path)
		ProjectName.text = godot_project_name + " (" + godot_project_path + ")"
		DataProgressBar.value = 0
		DataProgressBar.visible = true
		ResourceList.clear()
		
		# The process of enacting the progresds bar goes
		# 0) Check average values in options file. Backfill defaults if none.
		# 1) Calculate how many files
		# 2) Guess at work effort time
		# 3) Start timer
		# 4) Process files
		# 5) End timer and compute new average work effort number
		# 6) Update values in options file
		
		EnsureEstimatingDataAvailable()
		var file_count = GetFileCount(godot_project_dir)
		var projected_work_effort_time_secs = ProjectedWorkEffort(file_count)
		EstablishProgressBar(projected_work_effort_time_secs)

		# Establish the animated hourglass
		var hourglass_cursor: AnimatedSprite = HourglassCursor.instance()
		self.add_child(hourglass_cursor)
		self.connect("cursor_off", hourglass_cursor, "_on_cursor_off")
		
		# Now get the end time for the work scope effort 
		var actual_work_effort_time:float = OS.get_unix_time() 
		
		# Proceed with doing work
		if in_debug_mode:
			# We are in debug mode, so no threads!
			GenerateResourceList(godot_project_dir)
		else:
			
			# Let's use threads to process stuff in the background
			DataExtractionThread.start(self, "GenerateResourceList", godot_project_dir)
		
		# Compute the difference in work start time to end time
		actual_work_effort_time = OS.get_unix_time() - actual_work_effort_time + 0.001 # Ensure at least tiny second, otherwse too hard to compute thingds
		
		# Contribute to the calibration for the next cycle
		CalibrateWorkEffort(file_count, actual_work_effort_time)
	
func _on_data_extraction_progress_timer_timeout():
	DataProgressBar.value = DataProgressBar.value + DataProgressBar.step

func EstablishProgressBar(work_effort_time_secs: float):
	# Sets up the progress bar to the correct step click counter to 
	# match the time_wait on the timer
	# Setup the progres bar
	DataProgressBar.min_value = 0
	DataProgressBar.max_value = work_effort_time_secs
	DataProgressBar.step = work_effort_time_secs/100
	# Set up a timer to update progress bar according to this measure
	data_extraction_progress_timer.wait_time = DataProgressBar.step
	data_extraction_progress_timer.one_shot = false
	data_extraction_progress_timer.connect("timeout", self, "_on_data_extraction_progress_timer_timeout") 
	self.add_child(data_extraction_progress_timer) # add to process
	data_extraction_progress_timer.start()

func EnsureEstimatingDataAvailable():
	# Will check to see if there are values fhe estimates
	if average_search_time_100_files <= 1.0:
		# We have no value for a 100 files project set one and assign to optons
		average_search_time_100_files = 1.0/100.0
		
	if average_search_time_1000_files <= 10.0:
		# We have no value for a 100 files project set one and assign to optons
		average_search_time_1000_files = 10.0/100.0
		
	if average_search_time_10000_files <= 100.0:
		# We have no value for a 100 files project set one and assign to optons
		average_search_time_10000_files = 100.0/1000.0
		
	SaveOptions()
		

func ConfirmGodotProject(project_path: String) -> bool:
	# This will confirm that this is a godot project by the existence of the project.godot file and that it's readable
	var file_resource = File.new()
	var file_path:String =  project_path + "project.godot"
	var file_exists:bool = false
	file_exists = file_resource.file_exists(file_path)
	if file_exists:
		# Check if we can read the file 
		var file = File.new()
		var err = file.open(file_path, File.READ)
		if err != OK:
			print("Failure opening 'project.godot' file. Cannot open project file. Please check read permissions.")
			file.close()
			file_exists = false
			return file_exists
		else:
			file_exists = true
			return file_exists
	else:
		print("Cannot find " + file_path +" file. Is this a Godot project?")
		file_exists = false
		return file_exists
		
	# Ottherwis, project file exists and can be open, so we return true
	file_exists	= true
	return file_exists
	
func GetGodotProjectName(project_path: String) -> String:
	# Fetched the project name from 
	var file = File.new()
	file.open(project_path + "project.godot", File.READ)
	var content = file.get_as_text()
	file.close()
	
	# We're looking for a string formatted like this: config/name="Godot Unused Resource Scanner"
	var pos_start = content.find('config/name="') + 'config/name="'.length()
	var pos_end = content.find('"', pos_start)
	var project_name = content.substr(pos_start, pos_end - pos_start) 
	
	return project_name
	
func GenerateResourceList(godot_project_dir: String):
	print("GenerateResourceList()")
	# Given a directory, will determine the complete list of files which match the filter creteria for Godot resources
	# Here are the criteria:
	# 1) All kinds of standard godot resources are considered viable
	# 2) For each resource found in the project, we have to scan all of the project files 
	#    which might rererence these resources and determine if they're being referenced in those resources
	# 3) If no reference to the current resource is found, then it is dispplayed in the unreferenced resources list
	

	
	# Let's get every file in the project
	var full_dir_file_list = GetFilelist((godot_project_dir))

	# Let's figure out which are the files which are iikely resource files or have references to resource files
	var filtered_file_list = FilterFileList(full_dir_file_list)
	# Let's check to see if any of the filtered file list appears in the contents of the full dir file list 
	resources_file_list.clear()
	resources_file_list = ExtractResourceFileList(filtered_file_list, full_dir_file_list)
	#var unreferenced_resources_file_list = ExtractUnreferencedResourcesList(resources_file_list)
	# Now let's popuilate the list
	PopulateResourceList(resources_file_list)
	
	# Let's compute the work effort for this scope of works. 
	# We obvously only do that if we had files to work with
	var file_count = full_dir_file_list.size()
	
	
		
	# Finally, switch off the timer and reset the prgress bar to 100%
	DataProgressBar.value = DataProgressBar.max_value
	DataProgressBar.visible = false
	# Let's stop the progress bar timer
	data_extraction_progress_timer.stop()
	# Let's also clear the ResourcesForRemoval text area in preparation for the resources being placed there soon
	ResourcesForRemoval.text = ""
	
	# Kill off the cool hourglass cursor, in case it's spinning
	emit_signal("cursor_off")
	if !in_debug_mode:
		DataExtractionThread.wait_to_finish()

func ProjectedWorkEffort(file_count: int) -> float:
	# Given a metric of files, will guess at the at the projected time effort in seconds to 
	# process these files and return it.
	var projected_work_effort: float = 0.0
	if file_count > 0:
		var guess_work_effort: float
		# Let's figure oue which tranche of work effort this batch belongs to
		if file_count <= 100:
			projected_work_effort = average_search_time_100_files * file_count 
		elif file_count >= 100 and file_count <= 1000:
			projected_work_effort = average_search_time_1000_files * file_count
		else:
			projected_work_effort = average_search_time_10000_files * file_count
		
	return projected_work_effort
	

func CalibrateWorkEffort(file_count: int, actual_work_time: float):
	# Given a metric of files, will calculate the time effort in seconds to 
	# process these filesand return it.
	# Get the start time for the work efforet
	 
	var caliibrated_work_effort:float = 0.0
	
	if file_count > 0:
		# Let's compute how muuch calibratedd time it would have taken to process 100/1000/10000 files equivalent
		if file_count <= 100:
			caliibrated_work_effort = float(actual_work_time * 100.0/file_count)
			average_search_time_100_files = caliibrated_work_effort 
		elif file_count >= 100 and file_count <= 1000:
			caliibrated_work_effort = float(actual_work_time * 1000.0/file_count)
			average_search_time_1000_files = caliibrated_work_effort * 1000.0/file_count
		else:
			caliibrated_work_effort = float(actual_work_time * 10000.0/file_count)
			average_search_time_10000_files = caliibrated_work_effort * 10000.0/file_count
			
		# Now that we have the updated values, sve them
		SaveOptions()
	
func PopulateResourceList(file_list: Dictionary) -> int:
	# Given an array, will populate the item list
	# First, clear the list
	ResourceList.clear()
	for file in file_list.keys():
		if file_list[file][2] == 0:
			# If we have no reference count, them we are an orphan, unless we're in the never orphaned list
			if !never_orphaned_files.has(file):
				ResourceList.add_item(file)
				#print(file)
	# Return the size of the resource list
	return ResourceList.get_item_count()
		
	
func ExtractResourceFileList(file_list: Dictionary, full_file_list: Dictionary) -> Dictionary:
	# Given a dictionary, and the full list of files in our Godot file-space, we will iterate through each file, 
	# looking for both short and long-form (Godot) versions of the file & file path
	var resource_files : Dictionary = {}

	# Given a list of files in the full_file_list, open each text file and iterate through the contents of the 
	# file_list that we're looking for, and adding a reference to the resource_files for each one we find
	# *not* referenced by any other file than itself.
	# We scan for both the 	filename and a Godot project res:// path + filename, scan all eligible files in that 
	# project to see if any file (outside itself) references that resource
	var file = File.new()
	var content: String = ""
	var full_file_path: String = ""
	var scanning_file_path: String = ""
	var first_line: String = ""
	
	for scanning_res_path in full_file_list.keys():
		# We have the full list of complete file-paths, so let's iterate through each file, open it, 
		# then iterate through both the list of file-paths we're scanning for and list of just the filenames
		# From this, we forget the ones which are referenced by any file object not themselves, and build 
		# a dictionary of the filepaths and file names which aren't referenced, thus orphan candidates 
		
		# Let's first make sure we're not going to scan that file we're searching for, as that defeats
		# the purpose of havimg other files reference it
		#print("scanning_res_path: ", scanning_res_path)
	
		# Let's get the filename and the full path
		var scanning_file_name = full_file_list[scanning_res_path][0]
		var scanning_full_path = full_file_list[scanning_res_path][1]
		var file_is_not_scannable = false
		#print("scanning_full_path: ", scanning_full_path)
		# We then make sure that this isn't one of the kinds of files that will not contain content we scan for
		for regex_criterion in exclude_scanning_files_regexes:
			
			#print("regex_criterion: ", regex_criterion)
			regex.compile(regex_criterion)
			var matches
			if regex.is_valid():
				# We need to compare the filename against the regex exclusions
				matches = regex.search(scanning_file_name)
				if matches != null:
					# We are here because this *is* a file which matches one of the exclusion regexes, 
					# We therefore set a flag to say that at least one of the excludes regexes has failed
					# and that we cannottherefore use this file
					file_is_not_scannable = true
					
		# Then we check to see if this file hasn't been excluded *and* that it exists
		if !file_is_not_scannable and file.file_exists(scanning_full_path):
			file.open(scanning_full_path, File.READ)
			# Let's find out if this is a text file
			content = file.get_as_text()
			file.close()
			# Let's now loop through every file from our interest file list, and determine if it's present. If no to, 
			# we add it to our orphan resources dictionary
			for file_res_path in file_list.keys():
				if file_res_path.find("tank") >= 0:
					pass
				var counter
				# Iterate through the res://path/to/another/file1.gd
				# And see if any of the dictionary elements are present in this file at all. *IF NOT*, add this to our resources dictionary
				#print("Finding: ", file_res_path, ", in file: ", scanning_full_path) 
				if file_res_path in content  or file_list[file_res_path][0] in content  or file_list[file_res_path][1] in content or SubstringMatch(file_list[file_res_path][0], content):
					# This resource is referenced in some way in the current file. Let's imcreent its resource counter
					# Have we already added this resource? if yes, increment it
					if resource_files.has(file_res_path):
						# Let's just increment the counter
						counter =  resource_files[file_res_path][2]
						counter += 1
						resource_files[file_res_path][2] = counter
					else:
						# Otherwise set the count to one
						resource_files[file_res_path] = [file_list[file_res_path][0], file_list[file_res_path][1], 1]
				else:
					if !resource_files.has(file_res_path):
						# Otherwise establish the resource entry with zero count
						resource_files[file_res_path] = [file_list[file_res_path][0], file_list[file_res_path][1], 0]

					
	return resource_files
	
#func ExtractUnreferencedResourcesList(file_list: Dictionary) -> Dictionary:
#	var unreferenced_resource_files : Dictionary = {}
#	return unreferenced_resource_files

	
func FilterFileList(file_list: Dictionary) -> Dictionary:
	print("FilterFileList()")
	# Given an unfiltered file list, iterate through each item and see if it matches any of the regexes
	# RegExp resources
	# https://gdscript.com/solutions/regular-expressions/
	# https://cheatography.com/davechild/cheat-sheets/regular-expressions/
	# https://www.youtube.com/watch?v=DypnsSgC2Tw&t=465s
	# https://regex101.com
	var filtered_files : Dictionary = {}
	#print(file_list)
	var exclude_resources_regex = RegEx.new()
	var exclude_matches
	var matches
	for regex_criterion in resource_regexes:
		print("regex_criterion: ", regex_criterion)
		regex.compile(regex_criterion)
		if regex.is_valid():
			for key in file_list.keys():
				# Let's see if this file is one of the excluded files
				for exclude_resources_regex_criterion in exclude_resource_files_regexes:
					#print("exclude_resources_regex_criterion: ", exclude_resources_regex_criterion)
					exclude_resources_regex.compile(exclude_resources_regex_criterion)
					if exclude_resources_regex.is_valid():
						# Firsst, we scan for the key which is the godot name
						exclude_matches = exclude_resources_regex.search(key)	
						if exclude_matches == null:
							# This kind of file isn't excluded by our exlude regexes, so let's proceed with processing it
							#print("File not excluded: ", key)
							
							# If alse, then we scan for the filename
							matches = regex.search(file_list[key][0])
							if matches != null:
								#print("Filter file_list[key][0]: ", file_list[key][0])
								for result in matches.get_strings():
									filtered_files[key] = [file_list[key][0], file_list[key][1]]

							# If alse, then we scan for the filename and path
							matches = regex.search(file_list[key][1])
							#print("Filter file_list[key][1]: ", file_list[key][1])
							if matches != null:
								for result in matches.get_strings():
									filtered_files[key] = [file_list[key][0], file_list[key][1]]
							
				
	
	return filtered_files

func GetFileCount(scan_dir: String) -> int:
	#print("GetFileCount: ", scan_dir)
	# This comes from here: https://godotengine.org/qa/30423/files-folders-subfolders-outside-project-directory-godot
	var dir := Directory.new()
	var file_count: int = 0
	
	if dir.open(scan_dir) != OK:
		printerr("Warning: could not open directory: ", scan_dir)
		return 0
		
	if dir.list_dir_begin(true, true) != OK:
		printerr("Warning: could not list contents of: ", scan_dir)
		return 0

	var file_name := dir.get_next()
	
	while file_name != "":
		
		if dir.current_is_dir():
			file_count = file_count +  GetFileCount(dir.get_current_dir() + "/" + file_name)
		else:
			file_count += 1
		
		file_name = dir.get_next()
			
	return file_count
	
func GetFilelist(scan_dir: String) -> Dictionary:
	#print("GetFilelist: ", scan_dir)
	# This comes from here: https://godotengine.org/qa/30423/files-folders-subfolders-outside-project-directory-godot
	var my_files : Dictionary = {}
	var dir := Directory.new()
	if dir.open(scan_dir) != OK:
		printerr("Warning: could not open directory: ", scan_dir)
		return {}
		
	if dir.list_dir_begin(true, true) != OK:
		printerr("Warning: could not list contents of: ", scan_dir)
		return {}

	var file_name := dir.get_next()
	while file_name != "":
		if dir.current_is_dir():
			var next_level_dict: Dictionary =  GetFilelist(dir.get_current_dir() + "/" + file_name)
			#print("next_level_dict: ", next_level_dict)
			my_files = MergeDict(my_files, next_level_dict)
		else:
			# We create a dictionary that looks like this: 
			# { "res://path/to/file1.gd" : ["file1.gd",  "/home/user/game/path/to/file1.gd"]
			#	"res://path/to/file2.gd" : ["file2.gd", "/home/user/game/path/to/file2.gd"]
			#	"res://path/to/another/file1.gd" : ["file1.gd", "/home/user/game/path/to/another/file1.gd"]
			# }
			# This approach allows us to key off unique Godot IDs and full OS file path and retain the filename
			var full_file_path: String = dir.get_current_dir() + "/" + file_name
			# We now want to convert: /home/user/game/path/to/another/file1.gd to res://path/to/another/file1.gd
			# by knowing that /home/user/game/ is the godot project path
			# This, let's look for the subsring adter res:/ and make that + godot project the full file path
			var partial_path: String = full_file_path.substr(full_file_path.find(godot_project_path) + godot_project_path.length(), full_file_path.length() -1)
			# We should now have /path/to/file1.gd
			var godot_res_path: String = "res://" + partial_path 
			# we should now have res://path/to/file1.gd"
			
			my_files[godot_res_path] = [file_name, full_file_path]
			# We should now have: "res://path/to/file1.gd" : ["file1.gd",  "/home/user/game/path/to/file1.gd"]
			
			#my_files.append(dir.get_current_dir() + "/" + file_name)
			#print("file_name: ", file_name)
		
		file_name = dir.get_next()
			
	return my_files
	
func MergeArray(array_1: Array, array_2: Array, deep_merge: bool = false) -> Array:
	var new_array = array_1.duplicate(true)
	var compare_array = new_array
	var item_exists
	
	if deep_merge:
		compare_array = []
		for item in new_array:
			if item is Dictionary or item is Array:
				compare_array.append(JSON.print(item))
			else:
				compare_array.append(item)
				
	for item in array_2:
		item_exists = item
		if item is Dictionary or item is Array:
			item = item.duplicate(true)
			if deep_merge:
				item_exists = JSON.print(item)
			if not item_exists in compare_array:
				new_array.append(item)
	return new_array
	
func MergeDict(dict_1: Dictionary, dict_2: Dictionary, deep_merge: bool = false) -> Dictionary:
	# https://stackoverflow.com/questions/71415669/how-to-merge-two-dictionaries-or-arrays-in-gdscript-godot-engine
	var new_dict = dict_1.duplicate(true)
	for key in dict_2:
		if key in new_dict:
			if deep_merge and dict_1[key] is Dictionary and dict_2[key] is Dictionary:
				new_dict[key] = MergeDict(dict_1[key], dict_2[key])
			elif deep_merge and dict_1[key] is Array and dict_2[key] is Array:
				new_dict[key] = MergeArray(dict_1[key], dict_2[key])
			else:
				new_dict[key] = dict_2[key]
		else:
			new_dict[key] = dict_2[key]
	return new_dict

	
func PlayMediaType(file_name: String, file_path: String) -> bool:
	print("PlayMediaType(file_path): ", file_path)
	
	# Let's check to see if this is a PNG. WEBM. JPG file and show it:
	var media_regex = RegEx.new()
	media_regex.compile("(?i)\\w+\\.png|(?i)\\w+\\.webm|(?i)\\w+\\.jpg|(?i)\\w+\\.svg|(?i)\\w+\\.webm")
	if media_regex.is_valid():
		# We need to compare the filename against the regex exclusions
		var matches = media_regex.search(file_name)
		if matches != null:
			var preview_image = Image.new()
			preview_image.load(file_path)
			var preview_texture = ImageTexture.new()
			preview_texture.create_from_image(preview_image)
			ImagePreview.texture = preview_texture
			return true
			
		
	# otherwise, we let the shell open it:
	OS.shell_open(file_path)
	
#	media_regex.compile("\\w+\\.ogg|\\w+\\.wav|\\w+\\.mp3")
#	if media_regex.is_valid():
#		# We need to compare the filename against the regex exclusions
#		var matches = media_regex.search(file_name)
#		if matches != null:
#			var audio_track : AudioStream = load("user://Documents/development/a_test_project_for_unused_scanner/assets/Black_Devils-MonkymanREDO_11KHz.ogg")
#			var audio_player = AudioStreamPlayer.new()
#			add_child(audio_player)
#			audio_player.set_stream(audio_track)
#			audio_player.play()
#			return true
	
	return false

func AddResourcesRemoval(file_name: String, file_path: String):
	# Given a filename and full path, will move that resource from the ResourcesList over to the
	# Resoources for removal text area
	
	print("AddResourcesRemova(file_path): ", file_path)
	if ResourcesForRemoval.text.length() > 0:
		# This is not the first entry. Add a newline
		ResourcesForRemoval.text = ResourcesForRemoval.text + "\n"
		
	ResourcesForRemoval.text = ResourcesForRemoval.text + file_path +  " --> " + RECOVERY_STORAGE_STR  + file_name
	
func _on_ResourceList_item_activated(index):
	print("_on_ResourceList_item_activated()")
	AddResourcesRemoval(resources_file_list[ResourceList.get_item_text(index)][0], resources_file_list[ResourceList.get_item_text(index)][1])
	# As part of the step to move this resource from the list to the removal holding text area. 
	# we need to remove it from the list.
	ResourceList.remove_item(index)

func _on_ResourceList_multi_selected(index, selected):
	# First, check to see if the user is pressing control or shift keys, which means they want to select only, not play
	if  !Input.is_key_pressed(KEY_SHIFT) and !Input.is_key_pressed(KEY_CONTROL):
		# Let's iterate trough the itemlist and find and play each selected index
		PlayMediaType(resources_file_list[ResourceList.get_item_text(index)][0], resources_file_list[ResourceList.get_item_text(index)][1])


func SubstringMatch(file_name: String, file_content: String) -> bool:
	# This function is passed a filename and will look through both left and right portions of the filename within the passed content
	var substr_criteria_left: String = ""
	var substr_criteria_right: String = ""
	
	# given LevelAbove16.tscn
	# match LevelAbove1* or *bove16.tscn
	
	print("file_name: ", file_name)
	
	# First we search for the left substring
	if file_name.length() > substring_search_sensitivity:
		# our file name is large enough to grab the substring of
		substr_criteria_left = file_name.left(substring_search_sensitivity)
		substr_criteria_right = file_name.right(file_name.length() - substring_search_sensitivity) 
		print ("Substr: ", substr_criteria_left, " --  ", substr_criteria_right)
	
		if file_content.find(substr_criteria_left) > -1 or file_content.find(substr_criteria_right) > -1:
			return true
	
	return false


func _on_Open_pressed():
	# Let's iterate trough the itemlist and find and play each selected index
	for index in range(ResourceList.get_item_count()):
		# iterate through the itemlist and play all tracks
		if ResourceList.is_selected(index):
			print("index: ", resources_file_list[ResourceList.get_item_text(index)][0])
			PlayMediaType(resources_file_list[ResourceList.get_item_text(index)][0], resources_file_list[ResourceList.get_item_text(index)][1])
	

func _on_AddForRemoval_pressed():
	# Let's iterate trough the itemlist and find and play each selected index
	# And show line numbers in it
	# We reset the label, in case there had been a previous recovery operation
	RemovalLabel.text = "Resources to be Removed"
	ResourcesForRemoval.show_line_numbers = true
	for index in range(ResourceList.get_item_count()):
		# iterate through the itemlist and play all tracks
		if ResourceList.is_selected(index):
			AddResourcesRemoval(resources_file_list[ResourceList.get_item_text(index)][0], resources_file_list[ResourceList.get_item_text(index)][1])
			
	# As part of the step to move this resource from the list to the removal holding text area. 
	# we need to remove it from the list. We do it this way so we don't eat the entries from the 
	# itemlist from under ourselves.
		
	var num_resources_for_removal = ResourceList.get_item_count()
	for index in range(num_resources_for_removal, 0, -1):
		# iterate through the itemlist and remove each resource
		if ResourceList.is_selected(index - 1):
			ResourceList.remove_item(index - 1)


func _on_ClearAll_pressed():
	# Clear the text box
#	for resource in ResourcesForRemoval.text.split("\n"):
#		ResourceList.add_item(resource)
	# Repopulate the ResourceList from the dictionary
	PopulateResourceList(resources_file_list)
	ResourcesForRemoval.text = ""
	ResourcesForRemoval.show_line_numbers = false


func _on_MoveToRubbish_pressed():
	# Let's iterate through all line numbers and move the files to the holding pen for garbage
	var resources_for_removal = []
	
	# We only want to process the removal bin if there are line numbers
	if ResourcesForRemoval.show_line_numbers:
		if ResourcesForRemoval.get_line_count() > 0:
			var num_resources_for_removal = ResourcesForRemoval.get_line_count()
			for index in range(num_resources_for_removal, 0, -1):
				resources_for_removal.append(ResourcesForRemoval.get_line(index - 1))
				
		RemoveResource(resources_for_removal)
		ResourcesForRemoval.text = ""
		ResourcesForRemoval.show_line_numbers = false


func RemoveResource(resources_for_removal: Array):
	# Given a filename, will move resource plus any associated Godot artifact (.import files) to the garbage pen
	# It will put this in a date and project specific directory
	# It will also create a file with instructions on how to reverse this whole process

	var timeDict:Dictionary = OS.get_datetime()
	var curremt_storage_dir = resource_holding_pen + godot_project_name.replace(" ","_") + "/" + str(timeDict.year) + "_" + str(timeDict.month) + "_" + str(timeDict.day)\
		 + "_" + str(timeDict.hour) + "_" + str(timeDict.minute) + "_" + str(timeDict.second) + "/"
	
	var dir = Directory.new()
	# Let's make sure the holding pen exists
	dir.make_dir_recursive(curremt_storage_dir)
	var from_file_path: String
	var to_file_path: String
	var recovery_file_text: String
	var recovery_file: String = "godot_unused_resource_scaner_recovery_file.godot_recovery"
	for resource in resources_for_removal:
		from_file_path = resource.left(resource.find("-->") - 1).strip_edges()
		to_file_path = resource.right(resource.find("-->") + "-->".length()).strip_edges() 
		to_file_path = curremt_storage_dir + to_file_path.replace(RECOVERY_STORAGE_STR,"")
		dir.rename(from_file_path, to_file_path)
		
		# Now let's build the recovery file
		recovery_file_text = recovery_file_text + to_file_path + " --> " + from_file_path + "\n"
		
	# Write out recovery file
	var file = File.new()
	file.open(curremt_storage_dir + recovery_file, File.WRITE)
	file.store_string(recovery_file_text)
	file.close()
    
func _on_Recover_pressed():
	$RecoveryFileChooser.popup_centered()

func _on_RecoveryFileChooser_file_selected(path):
	RecoverResources(path)
	$RecoveryFileChooser.hide()
	

func RecoverResources(path):
	# Given a file-path, will read the backup recovery file and action it, reversing a pervious removal
	var file = File.new()
	var dir = Directory.new()
	# Let's make sure the holding pen exists
	
	
	file.open(path, File.READ)
	var content = file.get_as_text()
	file.close()
	var from_file_path: String
	var to_file_path: String
	
	#dir.make_dir_recursive(curremt_storage_dir)
	for recovery_line in content.split("\n"):
		# Iterate through each line, extracting the paths and actioning the recovery
		from_file_path = recovery_line.left(recovery_line.find("-->") - 1).strip_edges()
		to_file_path = recovery_line.right(recovery_line.find("-->") + "-->".length()).strip_edges()
		
		dir.rename(from_file_path, to_file_path)
	
		# Change the resources label and populat the resources for removal textedit with this informatoin
		RemovalLabel.text = "Recovered resources"		
		if to_file_path.length() > 0:
			ResourcesForRemoval.text = ResourcesForRemoval.text +  to_file_path + "\n"

func SaveOptions():
	#prepare a json and save it
	var options_data = {
		"substring_search_sensitivity" : 7,
		"godot_projects_base_directory" : godot_projects_base_directory,
		"recovery_storage_base_directory" : recovery_storage_base_directory,
		"average_search_time_100_files" : average_search_time_100_files,
		"average_search_time_1000_files" : average_search_time_1000_files,
		"average_search_time_10000_files" : average_search_time_10000_files
		}
	
	var file = File.new()
	file.open("user://options_data.json", File.WRITE)
	file.store_line(to_json(options_data))
	file.close()
	
func LoadOptions():
	
	var file = File.new()
	if not file.file_exists("user://options_data.json"):
		return
		
	file.open("user://options_data.json", File.READ)
	var options_data = parse_json(file.get_as_text())
	
	# Assign the local active vsriables with the config value, assuming it exists
	if "substring_search_sensitivity" in options_data: substring_search_sensitivity = options_data["substring_search_sensitivity"]
	if "godot_projects_base_directory" in options_data: godot_projects_base_directory = options_data["godot_projects_base_directory"]
	if "recovery_storage_base_directory" in options_data: recovery_storage_base_directory = options_data["recovery_storage_base_directory"]
	if "average_search_time_100_files" in options_data: average_search_time_100_files = options_data["average_search_time_100_files"]
	if "average_search_time_1000_files" in options_data: average_search_time_1000_files = options_data["average_search_time_1000_files"]
	if "average_search_time_10000_files" in options_data: average_search_time_10000_files = options_data["average_search_time_10000_files"] 
	
func _on_Options_pressed():
	var options_dialog = OptionsDialog.instance()
	self.add_child(options_dialog)
	options_dialog.get_node("MarginContainer/VBoxContainer/GodotProjects/Value").text = godot_projects_base_directory
	options_dialog.get_node("MarginContainer/VBoxContainer/RecoveryStorage/Value").text = recovery_storage_base_directory
	options_dialog.popup_centered()
