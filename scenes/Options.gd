extends WindowDialog

var godot_directory_chooser: bool = false
var recovery_directory_chooser: bool = false
onready var GodotUnusedResourceScanner = get_parent() #get_tree().get_root().find_node("GodotUnusedResourceScanner")

func _ready():
	GodotUnusedResourceScanner = get_tree().get_root().find_node("GodotUnusedResourceScanner")
	self.visible = true


func _on_Close_pressed():
	
	queue_free()


func _on_DirectoryChooser_dir_selected(dir):
	if recovery_directory_chooser:
		# we got invoked by the Recovery directory chooser
		$MarginContainer/VBoxContainer/RecoveryStorage/Value.text = dir
		get_parent().recovery_storage_base_directory  = dir
	else:
		$MarginContainer/VBoxContainer/GodotProjects/Value.text = dir
		get_parent().godot_projects_base_directory = dir
		
	godot_directory_chooser = false
	recovery_directory_chooser = false
	
func _on_GodotSelect_pressed():
	godot_directory_chooser = true
	$DirectoryChooser.popup_centered()


func _on_RecoverySelect_pressed():
	recovery_directory_chooser = true
	$DirectoryChooser.popup_centered()
