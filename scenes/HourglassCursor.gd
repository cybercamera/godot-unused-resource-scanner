extends AnimatedSprite

onready var hourglass_tween = Tween.new()
onready var hourglass_timer = Timer.new()

func _ready():
	# Set up the timer
	
	hourglass_timer.wait_time = 1 # The larger this number. the slower the progress
	hourglass_timer.one_shot = true
	hourglass_timer.connect("timeout", self, "_on_hourglass_timer_timeout") 
	self.add_child(hourglass_timer) # add to process
	
	hourglass_timer.start()
	hourglass_tween.interpolate_property(self, "rotation" , 0, PI, 2, Tween.TRANS_LINEAR, Tween.EASE_IN)
	hourglass_tween.connect("tween_completed", self, "_on_hourglass_tween_completed")
	self.add_child(hourglass_tween) # add to process
	hourglass_tween.start()
	
func _process(delta):
	self.global_position = get_global_mouse_position() + Vector2(25,25)


func _on_hourglass_timer_timeout():
	self.rotation =  lerp_angle(0, PI, 2)
	if self.rotation == 0:
		hourglass_tween.interpolate_property(self, "rotation" , PI, 0, 2, Tween.TRANS_LINEAR, Tween.EASE_IN)
	else:
		hourglass_tween.interpolate_property(self, "rotation" , 0, PI, 2, Tween.TRANS_LINEAR, Tween.EASE_IN)

	hourglass_tween.start()

func _on_hourglass_tween_completed(obj, key):
	hourglass_timer.start()

func _on_cursor_off():
	queue_free()
